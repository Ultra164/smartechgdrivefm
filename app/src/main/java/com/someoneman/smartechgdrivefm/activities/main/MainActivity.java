package com.someoneman.smartechgdrivefm.activities.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.someoneman.smartechgdrivefm.R;
import com.someoneman.smartechgdrivefm.fs.AbstractFileFragment;
import com.someoneman.smartechgdrivefm.gdrive.fragments.GdriveFilesFragment;
import com.someoneman.smartechgdrivefm.gdrive.fragments.GdriveSharedWithMeFilesFragment;
import com.someoneman.smartechgdrivefm.gdrive.fragments.GdriveTrashFilesFragment;
import com.someoneman.smartechgdrivefm.sdcard.fragments.RootFilesFragment;
import com.someoneman.smartechgdrivefm.sdcard.fragments.SdcardFilesFragment;
import com.someoneman.smartechgdrivefm.utils.Users;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private AbstractFileFragment mCurrentAbstractFileFragment;
    private DrawerLayout mDrawer;
    private TextView mTextViewAccountName;

    //region Overrides

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_ab_menu);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setElevation(0);
        }

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);

        mTextViewAccountName = (TextView)headerView.findViewById(R.id.accountName);
        mDrawer = (DrawerLayout)findViewById(R.id.drawer_layout);

        navigationView.setCheckedItem(R.id.nav_device_sdcard);

        Users.getInstance().init(this, this);

        if (Users.getInstance().isAuthorized())
            mTextViewAccountName.setText(Users.getInstance().getCredential().getSelectedAccountName());

        if (savedInstanceState == null)
            changeFylesFragment(new SdcardFilesFragment());
        else
            mCurrentAbstractFileFragment = (AbstractFileFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (!mCurrentAbstractFileFragment.onBackPressed()) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mCurrentAbstractFileFragment.onBackPressed())
                    return true;
                else {
                    mDrawer.openDrawer(GravityCompat.START);
                }

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        if (!item.isChecked()) {
            switch (item.getItemId()) {
                case R.id.nav_device_sdcard:
                    changeFylesFragment(new SdcardFilesFragment());

                    break;

                case R.id.nav_device_root:
                    changeFylesFragment(new RootFilesFragment());

                    break;

                case R.id.nav_gdrive_mydrive:
                    changeFylesFragment(new GdriveFilesFragment());

                    break;

                case R.id.nav_gdrive_multiple:
                    changeFylesFragment(new GdriveSharedWithMeFilesFragment());

                    break;

                case R.id.nav_gdrive_trash:
                    changeFylesFragment(new GdriveTrashFilesFragment());

                    break;
            }
        }

        mDrawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Users.GOOGLE_ACCOUNT_AUTHORIZE_ACTIVITY_RESULT_CODE:
                Users.getInstance().onActivityResult(requestCode, resultCode, data);

                if (Users.getInstance().isAuthorized())
                    mTextViewAccountName.setText(Users.getInstance().getCredential().getSelectedAccountName());

                break;

            default:
                mCurrentAbstractFileFragment.onActivityResult(requestCode, resultCode, data);

                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Users.GET_ACCOUNTS_PERMISSION_REQUEST_CODE:
                Users.getInstance().onRequestPermissionsResult(requestCode, permissions, grantResults);

                break;

            default:
                mCurrentAbstractFileFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);

                break;
        }
    }

    //endregion

    //region Private

    private void changeFylesFragment(AbstractFileFragment abstractFileFragment) {
        mCurrentAbstractFileFragment = abstractFileFragment;
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, mCurrentAbstractFileFragment).commit();
    }

    //endregion
}
