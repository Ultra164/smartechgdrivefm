package com.someoneman.smartechgdrivefm.activities.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.someoneman.smartechgdrivefm.fs.AbstractFile;
import com.someoneman.smartechgdrivefm.fs.AbstractFileNameComparator;
import com.someoneman.smartechgdrivefm.fs.AbstractFileTypeComparator;
import com.someoneman.smartechgdrivefm.views.AbstractFileLayout;
import com.someoneman.smartechgdrivefm.views.OnCardClickListener;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by ultra on 16.06.2016.
 */
public class FileListArrayAdapter
        extends RecyclerView.Adapter<FileListArrayAdapter.ViewHolder>
        implements View.OnClickListener, View.OnLongClickListener {

    public enum Sort {NameASC, NameDESC, TypeASC, TypeDESC}

    public static final String SAVE_INSTANCE = "FileListArrayAdapterItems";

    private Context mContext;
    private ArrayList<AbstractFile> mItems = new ArrayList<>();
    private OnCardClickListener mOnCardClickListener;

    public FileListArrayAdapter(Context context) {
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        AbstractFileLayout fileLayout = new AbstractFileLayout(mContext);
        fileLayout.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        return new ViewHolder(fileLayout);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.fileLayout.setFile(mItems.get(position));
        holder.fileLayout.setTag(position);
        holder.fileLayout.setOnClickListener(this);
        holder.fileLayout.setOnLongClickListener(this);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public void onClick(View v) {
        if (mOnCardClickListener != null) {
            int position = (int)v.getTag();

            mOnCardClickListener.onCardClicked(v, position);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (mOnCardClickListener != null) {
            int position = (int)v.getTag();

            mOnCardClickListener.onCardLongClicked(v, position);

            return true;
        }

        return false;
    }

    public void setItems(ArrayList<AbstractFile> abstractFiles) {
        mItems = abstractFiles;

        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        mItems.remove(position);

        notifyItemRemoved(position);
    }

    public void addItem(int position, AbstractFile abstractFile) {
        mItems.add(position, abstractFile);

        notifyItemInserted(position);
    }

    public ArrayList<AbstractFile> getItems() {
        return mItems;
    }

    public AbstractFile getItem(int position) {
        return mItems.get(position);
    }

    public void sort(Sort type) {
        switch (type) {
            case NameASC:
                Collections.sort(mItems, new AbstractFileNameComparator());

                break;

            case NameDESC:
                Collections.sort(mItems, Collections.reverseOrder(new AbstractFileNameComparator()));

                break;

            case TypeASC:
                Collections.sort(mItems, new AbstractFileTypeComparator());

                break;

            case TypeDESC:
                Collections.sort(mItems, Collections.reverseOrder(new AbstractFileTypeComparator()));

                break;
        }

        notifyDataSetChanged();
    }

    public void sort(Sort... params) {
        for (int i = 0; i < params.length; i++) {
            sort(params[i]);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public AbstractFileLayout fileLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            fileLayout = (AbstractFileLayout)itemView;
        }
    }

    //region Listeners

    public void setOnCardClickListener(OnCardClickListener onCardClickListener) {
        mOnCardClickListener = onCardClickListener;
    }

    //endregion

}
