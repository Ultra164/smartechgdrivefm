package com.someoneman.smartechgdrivefm.activities.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.someoneman.smartechgdrivefm.R;
import com.someoneman.smartechgdrivefm.fs.Path;
import com.someoneman.smartechgdrivefm.utils.Log;
import com.someoneman.smartechgdrivefm.views.OnCardClickListener;

import java.util.ArrayList;

/**
 * Created by ultra on 17.06.2016.
 */
public class PathListArrayAdapter
        extends RecyclerView.Adapter<PathListArrayAdapter.ViewHolder>
        implements View.OnClickListener, View.OnLongClickListener {

    public static final String SAVE_INSTANCE = "PathListArrayAdapterItems";

    private ArrayList<Path> mItems = new ArrayList<>();
    private OnCardClickListener mOnCardClickListener;

    public PathListArrayAdapter() {
        super();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.path_list_layout, parent, false);
        view.setClickable(true);
        view.setFocusable(true);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTextViewName.setText(mItems.get(position).getAbstractFile().getName());

        holder.mView.setTag(position);
        holder.mView.setOnClickListener(this);
        holder.mView.setOnLongClickListener(this);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public Path getItem(int position) {
        return mItems.get(position);
    }

    public ArrayList<Path> getItems() {
        return mItems;
    }

    public void setItems(ArrayList<Path> paths) {
        mItems = paths;

        notifyDataSetChanged();
    }

    public void addItem(Path item) {
        mItems.add(item);

        notifyItemInserted(mItems.size() - 1);
    }

    public void removeFrom(int from) {
        int length = mItems.size() - from;

        for (int i = mItems.size() - 1; i >= from; i--) {
            Log.I("%s", mItems.get(i).getAbstractFile().getName());
            mItems.remove(i);
        }

        notifyItemRangeRemoved(from, length);
    }

    @Override
    public void onClick(View v) {
        if (mOnCardClickListener != null) {
            int position = (int)v.getTag();

            mOnCardClickListener.onCardClicked(v, position);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (mOnCardClickListener != null) {
            int position = (int)v.getTag();

            mOnCardClickListener.onCardLongClicked(v, position);

            return true;
        }

        return false;
    }

    public void setOnCardClickListener(OnCardClickListener onCardClickListener) {
        mOnCardClickListener = onCardClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public View mView;
        public TextView mTextViewName;

        public ViewHolder(View itemView) {
            super(itemView);

            mView = itemView;
            mTextViewName = (TextView)itemView.findViewById(R.id.name);
        }
    }

}
