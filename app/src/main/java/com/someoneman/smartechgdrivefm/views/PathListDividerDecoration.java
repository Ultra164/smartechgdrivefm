package com.someoneman.smartechgdrivefm.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.someoneman.smartechgdrivefm.R;

/**
 * Created by ultra on 18.06.2016.
 */
public class PathListDividerDecoration extends RecyclerView.ItemDecoration {

    private Drawable mDrawable;

    public PathListDividerDecoration(Context context) {
        mDrawable = context.getResources().getDrawable(R.drawable.path_list_divider_decoration);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int top = parent.getPaddingTop();
        int bottom = parent.getHeight() - parent.getPaddingBottom();

        int childCount = parent.getChildCount();

        for (int i = 1; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int left = child.getLeft() + params.leftMargin;
            int right = left + mDrawable.getIntrinsicWidth();

            mDrawable.setBounds(left, top, right, bottom);
            mDrawable.draw(c);
        }
    }
}
