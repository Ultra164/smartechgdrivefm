package com.someoneman.smartechgdrivefm.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.format.DateFormat;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.someoneman.smartechgdrivefm.R;
import com.someoneman.smartechgdrivefm.fs.AbstractFile;

import java.util.Arrays;

/**
 * Created by ultra on 16.06.2016.
 */
public class AbstractFileLayout extends LinearLayout {

    private static final String[] IMAGE_EXTENSIONS = {"jpg", "png", "bmp", "raw", "tiff", "jpeg", "gif", "jp2"};
    private static final String[] PDF_EXTANSIONS = {"pdf", "djvu"};
    private static final String[] VIDEO_EXTENSIONS = {"wmv", "avi", "mp4", "3gp", "mkv"};
    private static final String[] AUDIO_EXTENSIONS = {"wma", "wav", "mp3", "flac", "alac", "ogg"};
    private static final String[] WORD_EXTENSIONS = {"doc", "docx"};
    private static final String[] PP_EXTENSIONS = {"ppt", "pptx"};
    private static final String[] EXCEL_EXTENSIONS = {"xls", "xlsx"};
    private static final String[] DOC_EXTENSIONS = {"txt", "c", "cpp", "java", "log", "sh"};

    private TextView mTextViewTitle, mTextViewDecription;
    private ImageView mImageViewType;

    public AbstractFileLayout(Context context) {
        super(context);

        initView();
    }

    private void initView() {
        inflate(getContext(), R.layout.file_view_layout, this);

        mTextViewTitle = (TextView)findViewById(R.id.title);
        mTextViewDecription = (TextView)findViewById(R.id.description);
        mImageViewType = (ImageView)findViewById(R.id.type_image);
    }

    public void setFile(AbstractFile abstractFile) {
        mTextViewTitle.setText(abstractFile.getName());
        mTextViewDecription.setText(
                String.format("%s %s",
                        getResources().getString(R.string.fyle_last_change),
                        DateFormat.getDateFormat(getContext()).format(abstractFile.lastModified()).toString()
                ));

        setIconFyle(abstractFile);
    }

    private void setIconFyle(AbstractFile abstractFile) {
        String extension = abstractFile.getExtension();
        Drawable iconDrawable = getResources().getDrawable(R.drawable.ic_file);

        if (abstractFile.isDirectory() && abstractFile.isSystem()) {
            iconDrawable = getResources().getDrawable(R.drawable.ic_folder_outline);
        } else if (abstractFile.isDirectory()) {
            iconDrawable = getResources().getDrawable(R.drawable.ic_folder);
        } else if (abstractFile.isSystem()) {
            iconDrawable = getResources().getDrawable(R.drawable.ic_file_hidden);
        } else if (Arrays.asList(IMAGE_EXTENSIONS).contains(extension)) {
            iconDrawable = getResources().getDrawable(R.drawable.ic_file_image);
        } else if (Arrays.asList(PDF_EXTANSIONS).contains(extension)) {
            iconDrawable = getResources().getDrawable(R.drawable.ic_file_pdf);
        } else if (Arrays.asList(VIDEO_EXTENSIONS).contains(extension)) {
            iconDrawable = getResources().getDrawable(R.drawable.ic_file_video);
        } else if (Arrays.asList(AUDIO_EXTENSIONS).contains(extension)) {
            iconDrawable = getResources().getDrawable(R.drawable.ic_file_music);
        } else if (Arrays.asList(WORD_EXTENSIONS).contains(extension)) {
            iconDrawable = getResources().getDrawable(R.drawable.ic_file_word);
        } else if (Arrays.asList(PP_EXTENSIONS).contains(extension)) {
            iconDrawable = getResources().getDrawable(R.drawable.ic_file_powerpoint);
        } else if (Arrays.asList(EXCEL_EXTENSIONS).contains(extension)) {
            iconDrawable = getResources().getDrawable(R.drawable.ic_file_excel);
        } else if (Arrays.asList(DOC_EXTENSIONS).contains(extension)) {
            iconDrawable = getResources().getDrawable(R.drawable.ic_file_document);
        }

        mImageViewType.setImageDrawable(iconDrawable);
    }
}
