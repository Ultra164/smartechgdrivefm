package com.someoneman.smartechgdrivefm.views;

import android.view.View;

/**
 * Created by ultra on 20.06.2016.
 */
public interface OnCardClickListener {
    void onCardClicked(View view, int position);
    void onCardLongClicked(View view, int position);
}
