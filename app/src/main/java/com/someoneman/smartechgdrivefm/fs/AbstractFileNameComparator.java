package com.someoneman.smartechgdrivefm.fs;

import java.util.Comparator;

/**
 * Created by ultra on 17.06.2016.
 */
public class AbstractFileNameComparator implements Comparator<AbstractFile> {

    public AbstractFileNameComparator() {
    }

    @Override
    public int compare(AbstractFile lhs, AbstractFile rhs) {
        return lhs.getName().toLowerCase().compareTo(rhs.getName().toLowerCase());
    }
}
