package com.someoneman.smartechgdrivefm.fs;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by ultra on 18.06.2016.
 */
public class Path implements Parcelable {
    private AbstractFile mAbstractFile;
    private int mFileListVerticalOffset;

    public Path(AbstractFile abstractFile, int recyclerListVerticalOffset) {
        mAbstractFile = abstractFile;
        mFileListVerticalOffset = recyclerListVerticalOffset;
    }

    public Path(Parcel parcel) {
        mAbstractFile = (AbstractFile)parcel.readParcelable(AbstractFile.class.getClassLoader());
        mFileListVerticalOffset = parcel.readInt();
    }

    public AbstractFile getAbstractFile() {
        return mAbstractFile;
    }

    public int getRecyclerListVerticalOffset() {
        return mFileListVerticalOffset;
    }

    public void setRecyclerListVerticalOffset(int recyclerListVerticalOffset) {
        mFileListVerticalOffset = recyclerListVerticalOffset;
    }

    public static AbstractFile[] getFilesFromPath(ArrayList<Path> path) {
        AbstractFile[] files = new AbstractFile[path.size()];

        for (int i = 0; i < path.size(); i++) {
            files[i] = path.get(i).getAbstractFile();
        }

        return files;
    }

    public static void remove(int from, int to, ArrayList<Path> path) {
        for (int i = to; i >= from; i--) {
            path.remove(i);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mAbstractFile, flags);
        dest.writeInt(mFileListVerticalOffset);
    }

    public static final Parcelable.Creator<Path> CREATOR = new Creator<Path>() {
        @Override
        public Path createFromParcel(Parcel source) {
            return new Path(source);
        }

        @Override
        public Path[] newArray(int size) {
            return new Path[size];
        }
    };
}
