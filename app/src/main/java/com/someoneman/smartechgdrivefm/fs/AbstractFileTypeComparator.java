package com.someoneman.smartechgdrivefm.fs;

import java.util.Comparator;

/**
 * Created by ultra on 17.06.2016.
 */
public class AbstractFileTypeComparator implements Comparator<AbstractFile> {
    @Override
    public int compare(AbstractFile lhs, AbstractFile rhs) {
        if (lhs.isDirectory() && !rhs.isDirectory())
            return -1;
        else if (!lhs.isDirectory() && rhs.isDirectory())
            return 1;

        return 0; //lhs.isDirectory() && rhs.isDirectory()
    }
}
