package com.someoneman.smartechgdrivefm.fs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.someoneman.smartechgdrivefm.R;
import com.someoneman.smartechgdrivefm.activities.main.FileListArrayAdapter;
import com.someoneman.smartechgdrivefm.dialogs.FileInfoDialog;
import com.someoneman.smartechgdrivefm.dialogs.FileOptionsDialog;
import com.someoneman.smartechgdrivefm.views.OnCardClickListener;
import com.someoneman.smartechgdrivefm.views.PathListDividerDecoration;
import com.someoneman.smartechgdrivefm.views.FileListDividerDecoration;
import com.someoneman.smartechgdrivefm.activities.main.PathListArrayAdapter;

import java.util.ArrayList;

public abstract class AbstractFileFragment extends Fragment implements
        SwipeRefreshLayout.OnRefreshListener {

    //region Views

    private SwipeRefreshLayout mSwipeRefresh;
    private RecyclerView mFileList;
    private RecyclerView mPathList;
    private RecyclerView.ItemDecoration mFileListItemDecoration;
    private RecyclerView.ItemDecoration mPathListItemDecoration;
    private ActionBar mActionBar;
    private FileListArrayAdapter mFileListArrayAdapter;
    private PathListArrayAdapter mPathArrayAdapter;
    private Snackbar mSnackbar;

    //endregion

    //region Vars

    private boolean mHasSavedInstanceState = false;

    //endregion

    //region Fragment overrides

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.abstractfile_fragment_layout, container, false);

        if (savedInstanceState != null)
            mHasSavedInstanceState = true;

        mFileListItemDecoration = new FileListDividerDecoration(getContext());
        mPathListItemDecoration = new PathListDividerDecoration(getContext());

        mSwipeRefresh = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh);
        mSwipeRefresh.setOnRefreshListener(this);

        mFileList = (RecyclerView)view.findViewById(R.id.recycler_list);
        mFileList.setHasFixedSize(true);
        mFileList.setLayoutManager(new LinearLayoutManager(getContext()));

        if (mFileList.getAdapter() != null) {
            mFileListArrayAdapter = (FileListArrayAdapter)mFileList.getAdapter();
        } else {
            mFileListArrayAdapter = new FileListArrayAdapter(getContext());
            mFileList.setAdapter(mFileListArrayAdapter);
        }

        mFileListArrayAdapter.setOnCardClickListener(mOnFileListCardClickListener);
        mFileList.addItemDecoration(mFileListItemDecoration);
        mFileList.setOnScrollListener(mOnFileListScrollListener);

        mPathList = (RecyclerView)getActivity().findViewById(R.id.pathLayout);
        mPathList.setHasFixedSize(true);
        mPathList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        if (mPathList.getAdapter() != null) {
            mPathArrayAdapter = (PathListArrayAdapter) mPathList.getAdapter();
            mPathArrayAdapter.removeFrom(0);
        } else {
            mPathArrayAdapter = new PathListArrayAdapter();
            mPathList.setAdapter(mPathArrayAdapter);
        }

        mPathArrayAdapter.setOnCardClickListener(mOnPathListCardClickListener);
        mPathList.addItemDecoration(mPathListItemDecoration);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        mActionBar.setTitle(getRootTitle());
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        mFileList.removeItemDecoration(mFileListItemDecoration);

        mPathList.removeItemDecoration(mPathListItemDecoration);
        mPathList.setVisibility(View.GONE);

        super.onDestroyView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mFileListArrayAdapter != null && mFileListArrayAdapter.getItemCount() > 0) //Сохранение списка файлов
            outState.putParcelableArrayList(FileListArrayAdapter.SAVE_INSTANCE, mFileListArrayAdapter.getItems());

        if (mPathArrayAdapter != null && mPathArrayAdapter.getItemCount() > 0) //Сохранение пути
            outState.putParcelableArrayList(PathListArrayAdapter.SAVE_INSTANCE, mPathArrayAdapter.getItems());
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if (savedInstanceState != null) {
            //Восстановление списка файлов
            ArrayList<AbstractFile> abstractFiles = savedInstanceState.getParcelableArrayList(FileListArrayAdapter.SAVE_INSTANCE);
            if (abstractFiles != null)
                mFileListArrayAdapter.setItems(abstractFiles);

            //Восстановление пути
            ArrayList<Path> paths = savedInstanceState.getParcelableArrayList(PathListArrayAdapter.SAVE_INSTANCE);
            if (paths != null) {
                AbstractFile currentAbstractFile = paths.get(paths.size() - 1).getAbstractFile();

                mPathArrayAdapter.setItems(paths);
                reloadLists(currentAbstractFile, currentAbstractFile.listFiles());
            }
        }
    }

    //endregion

    //region Getters

    protected ActionBar getActionBar() {
        return mActionBar;
    }

    //endregion

    //region Params

    public boolean isHasSavedInstanceState() {
        return mHasSavedInstanceState;
    }

    //endregion

    //region Private

    private void changeDirByFile(AbstractFile dir) {
        dismissSnackbar();

        if (dir.listFiles() == null) {
            Toast.makeText(getContext(), "Нет доступа к каталогу", Toast.LENGTH_LONG).show();

            return;
        }

        if (mPathArrayAdapter.getItemCount() > 0) {
            int verticalScrollOffset = mFileList.computeVerticalScrollOffset();
            mPathArrayAdapter.getItem(mPathArrayAdapter.getItemCount() - 1).setRecyclerListVerticalOffset(verticalScrollOffset);
        }
        mPathArrayAdapter.addItem(new Path(dir, 0));

        reloadLists(dir, dir.listFiles());

        mPathList.scrollToPosition(mPathArrayAdapter.getItemCount() - 1);
        mFileList.scrollToPosition(0);
    }

    private void changeDirByPathIndex(int pathIndex) {
        dismissSnackbar();

        Path pathPart = mPathArrayAdapter.getItem(pathIndex);

        int verticalScrollOffset = pathPart.getRecyclerListVerticalOffset();

        reloadLists(pathPart.getAbstractFile(), pathPart.getAbstractFile().listFiles());

        mPathArrayAdapter.removeFrom(++pathIndex);
        mPathList.scrollToPosition(mPathArrayAdapter.getItemCount());
        mFileList.scrollBy(0, verticalScrollOffset);
    }

    private void updateCurrentDir() {
        if (mPathArrayAdapter.getItemCount() <= 0)
            return;

        AbstractFile dir = mPathArrayAdapter.getItem(mPathArrayAdapter.getItemCount() - 1).getAbstractFile();

        if (!dir.isDirectory())
            return;

        if (dir.isAsync()) {
            startRefreshing();
            new AbstractFile.AsyncListFiles(mUpdateDirListFylesLoadedListener).execute(dir);
        } else {
            stopRefreshing();

            reloadLists(dir, dir.listFiles(true));
        }
    }

    private void reloadLists(AbstractFile abstractFile, ArrayList<AbstractFile> abstractFiles) {
        if (abstractFile.getParent() != null) {
            mActionBar.setTitle(abstractFile.getName());
            mActionBar.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
            mPathList.setVisibility(View.VISIBLE);
        }
        else {
            mActionBar.setTitle(getRootTitle());
            mActionBar.setHomeAsUpIndicator(R.drawable.ic_ab_menu);
            mPathList.setVisibility(View.GONE);
        }

        mFileListArrayAdapter.setItems(abstractFiles);
        mFileListArrayAdapter.sort(FileListArrayAdapter.Sort.NameASC, FileListArrayAdapter.Sort.TypeASC);
    }

    private void dismissSnackbar() { //Вызывает событие dismiss у Snackbar если тот отображается
        if (mSnackbar != null && mSnackbar.isShown())
            mSnackbar.dismiss();
    }

    private void fileDelete(final AbstractFile abstractFile, final int position) {
        mFileListArrayAdapter.removeItem(position);

        mSnackbar = Snackbar.make(getView(), getString(R.string.file_deleted), Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(R.string.undo).toUpperCase(), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mFileListArrayAdapter.addItem(position, abstractFile);
                    }
                })
                .setCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        if (event != Snackbar.Callback.DISMISS_EVENT_ACTION) {

                            if (abstractFile.isAsync()) {
                                new AbstractFile.AsyncDeleteFile(new AbstractFile.OnFileDeleted() {
                                    @Override
                                    public void onFileDeleted() {
                                    }

                                    @Override
                                    public void onFileDeleteError() {
                                        mFileListArrayAdapter.addItem(position, abstractFile);
                                    }
                                }).execute(abstractFile);
                            } else {
                                if (!abstractFile.delete()) {
                                    mFileListArrayAdapter.addItem(position, abstractFile);
                                }
                            }
                        }
                    }
                });
        mSnackbar.show();
    }

    //endregion

    //region Protected

    protected void startRefreshing() {
        mSwipeRefresh.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefresh.setRefreshing(true);
            }
        });
    }

    protected void stopRefreshing() {
        mSwipeRefresh.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefresh.setRefreshing(false);
            }
        });
    }

    protected void selectFileOptions(int which, AbstractFile file) {} //Abstract

    protected FileOptionsDialog createWorkWithFileDialogBuilder(AbstractFile abstractFile, int position) {
        return new FileOptionsDialog(getContext(), abstractFile, position, mOnFileOptionsItemListener);
    }

    protected void showWorkWithFileDialog(AbstractFile abstractFile, int position) {
        createWorkWithFileDialogBuilder(abstractFile, position).show();
    }

    //endregion

    //region Public

    public void changeDir(@NonNull AbstractFile dir) {
        if (!dir.isDirectory())
            return;

        if (!dir.isLoaded() && dir.isAsync()) {
            startRefreshing();
            new AbstractFile.AsyncListFiles(mChangeDirListFilesLoadedListener).execute(dir);
        } else {
            changeDirByFile(dir);
        }
    }

    public boolean onBackPressed() {
        int l = mPathArrayAdapter.getItemCount();

        if (mPathArrayAdapter.getItemCount() > 1) {
            changeDirByPathIndex(mPathArrayAdapter.getItemCount() - 2);

            return true;
        }

        return false;
    }

    //endregion

    //region Listeners

    @Override
    public void onRefresh() {
        dismissSnackbar();
        updateCurrentDir();
    }

    private RecyclerView.OnScrollListener mOnFileListScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            dismissSnackbar();
        }
    };

    private AbstractFile.OnListFilesLoaded mChangeDirListFilesLoadedListener = new AbstractFile.OnListFilesLoaded() {
        @Override
        public void onListFilesLoaded(AbstractFile dir) {
            stopRefreshing();
            changeDirByFile(dir);
        }

        @Override
        public void onListFilesLoadError() {
        }
    };

    private AbstractFile.OnListFilesLoaded mUpdateDirListFylesLoadedListener = new AbstractFile.OnListFilesLoaded() {
        @Override
        public void onListFilesLoaded(AbstractFile abstractFile) {
            stopRefreshing();
            reloadLists(abstractFile, abstractFile.listFiles());
        }

        @Override
        public void onListFilesLoadError() {
        }
    };

    private OnCardClickListener mOnFileListCardClickListener = new OnCardClickListener() {
        @Override
        public void onCardClicked(View view, int position) {
            AbstractFile file = mFileListArrayAdapter.getItem(position);

            if (file.isDirectory())
                changeDir(file);
            else
                file.open(getContext(), getActivity());
        }

        @Override
        public void onCardLongClicked(View view, int position) {
            showWorkWithFileDialog(mFileListArrayAdapter.getItem(position), position);
        }
    };

    private OnCardClickListener mOnPathListCardClickListener = new OnCardClickListener() {
        @Override
        public void onCardClicked(View view, int position) {
            changeDirByPathIndex(position);
        }

        @Override
        public void onCardLongClicked(View view, int position) {

        }
    };

    private FileOptionsDialog.OnFileOptionsItemListener mOnFileOptionsItemListener = new FileOptionsDialog.OnFileOptionsItemListener() {
        @Override
        public void onItemSelected(FileOptionsDialog dialog, int which, final AbstractFile file, final int position) {
            switch (which) {
                case 0:
                    new FileInfoDialog(getContext(), file).show();

                    break;

                case 1:
                    fileDelete(file, position);

                    break;

                default:
                    selectFileOptions(which - (dialog.getItemsCount() - 1), file);

                    break;
            }
        }
    };

    //endregion

    //region Abstracts

    protected abstract String getRootTitle();

    //endregion
}
