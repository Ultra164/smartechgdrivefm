package com.someoneman.smartechgdrivefm.fs;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.format.DateFormat;

import com.google.api.client.util.DateTime;
import com.someoneman.smartechgdrivefm.R;
import com.someoneman.smartechgdrivefm.gdrive.GdriveFile;
import com.someoneman.smartechgdrivefm.sdcard.SdcardFile;
import com.someoneman.smartechgdrivefm.utils.Log;

import java.util.ArrayList;

public class AbstractFile implements Parcelable {

    //region Vars

    private String mName;
    private long mLastModified = 0;
    private boolean mIsDirectory = false;
    private boolean mIsSystem = false;
    private long mSize = 0;
    private AbstractFile mParentAbstractFile;
    private ArrayList<AbstractFile> mAbstractFiles;

    //endregion

    //region Constructors

    public AbstractFile() {
    }

    public AbstractFile(@Nullable AbstractFile parent) {
        mParentAbstractFile = parent;
    }

    public AbstractFile(@NonNull Parcel parcel) {
        mName = parcel.readString();
        mLastModified = parcel.readLong();
        mIsDirectory = parcel.readInt() == 1;
        mIsSystem = parcel.readInt() == 1;
        mParentAbstractFile = parcel.readParcelable(AbstractFile.class.getClassLoader());
        mSize = parcel.readLong();
        //mAbstractFiles = (AbstractFile[])parcel.readParcelableArray(AbstractFile.class.getClassLoader());
    }

    //endregion

    //region Public

    public ArrayList<AbstractFile> listFiles() {
        return listFiles(false);
    }

    public ArrayList<AbstractFile> listFiles(boolean forcedUpdate) {
        if (mAbstractFiles == null || forcedUpdate)
            mAbstractFiles = loadListFiles();

        return mAbstractFiles;
    }

    public void open(Context context, Activity activity) {
    }

    public boolean delete() {
        return false;
    }

    //endregion

    //region Protected

    protected ArrayList<AbstractFile> loadListFiles() {
        return null;
    }

    //endregion

    //region Getters

    public AbstractFile getParent() {
        return mParentAbstractFile;
    }

    public String getExtension() {
        String nameInLowerCase = getName().toLowerCase();

        return nameInLowerCase.substring(nameInLowerCase.lastIndexOf(".") + 1);
    }

    public String getName() {
        if (mName == null || mName.isEmpty())
            return "/";

        return mName;
    }

    public long getSize() {
        return mSize;
    }

    public String getHtmlInfo(Context context) {
        String html = String.format(
                        context.getString(R.string.file_info_dialog_format),

                        getName(),
                        lastModifiedAsString(context),
                        isDirectory() ? context.getString(R.string.directory) : context.getString(R.string.file),
                        getSize()
                );

        if (!isDirectory())
            html += String.format(
                    context.getString(R.string.file_info_dialog_file_format),

                    getExtension()
            );

        return html;
    }

    //endregion

    //region Protected setters

    protected void setName(String name) {
        mName = name;
    }

    protected void setLastModified(DateTime dateTime) {
        setLastModified(dateTime.getValue());
    }

    protected void setLastModified(Long value) {
        mLastModified = value;
    }

    protected void setIsDirectory(boolean value) {
        mIsDirectory = value;
    }

    protected void setIsSystem(boolean value) {
        mIsSystem = value;
    }

    protected void setSize(Long size) {
        if (size == null)
            mSize = 0;
        else
            mSize = size;
    }

    //endregion

    //region Params

    public boolean isDirectory() {
        return mIsDirectory;
    }

    public boolean isSystem() {
        return mIsSystem;
    }

    public boolean isAsync() {
        return false;
    }

    public long lastModified() {
        return mLastModified;
    }

    public String lastModifiedAsString(Context context) {
        return DateFormat.getDateFormat(context).format(lastModified());
    }

    public DateTime lastModified(boolean asDateTime) {
        return new DateTime(lastModified());
    }

    public boolean isLoaded() {
        return mAbstractFiles != null;
    }

    //endregion

    //region Parcelable

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
        dest.writeLong(mLastModified);
        dest.writeInt(mIsDirectory ? 1 : 0);
        dest.writeInt(mIsSystem ? 1 : 0);
        dest.writeParcelable(mParentAbstractFile, flags);
        dest.writeLong(mSize);
        //dest.writeParcelableArray(mAbstractFiles, flags);
    }

    public static final Parcelable.Creator<AbstractFile> CREATOR = new Creator<AbstractFile>() {
        @Override
        public AbstractFile createFromParcel(Parcel source) {
            switch (source.readInt()) {
                case 2:
                    return new GdriveFile(source);

                case 1:
                    return new SdcardFile(source);

                default:
                    return new AbstractFile(source);
            }
        }

        @Override
        public AbstractFile[] newArray(int size) {
            return new AbstractFile[size];
        }
    };

    //endregion

    //region Async tasks

    public static class AsyncListFiles extends AsyncTask<AbstractFile, Void, ArrayList<AbstractFile>> {

        private OnListFilesLoaded mOnListFilesLoaded;
        private AbstractFile mAbstractFile;

        public AsyncListFiles(OnListFilesLoaded onListFilesLoaded) {
            mOnListFilesLoaded = onListFilesLoaded;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected ArrayList<AbstractFile> doInBackground(AbstractFile... params) {
            mAbstractFile = params[0];

            return mAbstractFile.listFiles(true);
        }

        @Override
        protected void onPostExecute(ArrayList<AbstractFile> abstractFiles) {
            if (abstractFiles != null)
                mOnListFilesLoaded.onListFilesLoaded(mAbstractFile);
            else
                mOnListFilesLoaded.onListFilesLoadError();
        }
    }

    public static class AsyncDeleteFile extends AsyncTask<AbstractFile, Void, Boolean> {

        private OnFileDeleted mOnFileDeleted;

        public AsyncDeleteFile(OnFileDeleted onFileDeleted) {
            mOnFileDeleted = onFileDeleted;
        }

        @Override
        protected Boolean doInBackground(AbstractFile... params) {
            AbstractFile abstractFile = params[0];

            return abstractFile.delete();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (mOnFileDeleted != null) {
                if (result)
                    mOnFileDeleted.onFileDeleted();
                else
                    mOnFileDeleted.onFileDeleteError();
            }
        }
    }

    //endregion

    //region Interfaces

    public interface OnListFilesLoaded {
        void onListFilesLoaded(AbstractFile abstractFile);
        void onListFilesLoadError();
    }

    public interface OnFileDeleted {
        void onFileDeleted();
        void onFileDeleteError();
    }

    //endregion

}
