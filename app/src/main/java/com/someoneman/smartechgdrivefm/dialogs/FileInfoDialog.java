package com.someoneman.smartechgdrivefm.dialogs;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.someoneman.smartechgdrivefm.R;
import com.someoneman.smartechgdrivefm.fs.AbstractFile;

/**
 * Created by ultra on 20.06.2016.
 */
public class FileInfoDialog extends AlertDialog.Builder {

    public FileInfoDialog(Context context, AbstractFile file) {
        super(context);

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.file_info_dialog_layout, null);
        TextView textView = (TextView)view.findViewById(R.id.htmlText);

        textView.setText(Html.fromHtml(file.getHtmlInfo(getContext())));

        setTitle(file.getName());
        setView(view);
        setPositiveButton(getContext().getString(R.string.accept), null);
    }

}
