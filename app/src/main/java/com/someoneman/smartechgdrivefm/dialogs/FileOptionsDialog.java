package com.someoneman.smartechgdrivefm.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.someoneman.smartechgdrivefm.R;
import com.someoneman.smartechgdrivefm.fs.AbstractFile;

/**
 * Created by ultra on 20.06.2016.
 */
public class FileOptionsDialog extends AlertDialog.Builder {

    private AbstractFile mFile;
    private int mPosition;
    private OnFileOptionsItemListener mOnFileOptionsItemListener;
    private CharSequence[] mItems;

    public FileOptionsDialog(Context context, AbstractFile file, int position, OnFileOptionsItemListener onFileOptionsItemListener) {
        super(context);

        mFile = file;
        mPosition = position;
        mOnFileOptionsItemListener = onFileOptionsItemListener;

        setTitle(file.getName());

        if (mFile.isDirectory())
            mItems = getContext().getResources().getStringArray(R.array.folderDialogDefaultOptions);
        else
            mItems = getContext().getResources().getStringArray(R.array.fileDialogDefaultOptions);
    }

    public CharSequence[] getItems() {
        return mItems;
    }

    public int getItemsCount() {
        return mItems.length;
    }

    public void setItems(CharSequence[] items) {
        mItems = items;
    }

    @Override
    public AlertDialog show() {
        setItems(mItems, mFileDialogOnClickListener);

        return super.show();
    }

    //region Listeners

    private DialogInterface.OnClickListener mFileDialogOnClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();

            mOnFileOptionsItemListener.onItemSelected(FileOptionsDialog.this, which, mFile, mPosition);
        }
    };

    //endregion

    //region Interfaces

    public interface OnFileOptionsItemListener {
        void onItemSelected(FileOptionsDialog dialog, int which, AbstractFile file, int position);
    }

    //endregion
}
