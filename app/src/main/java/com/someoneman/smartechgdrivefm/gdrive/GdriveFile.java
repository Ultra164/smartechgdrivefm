package com.someoneman.smartechgdrivefm.gdrive;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.customtabs.CustomTabsIntent;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.someoneman.smartechgdrivefm.R;
import com.someoneman.smartechgdrivefm.fs.AbstractFile;
import com.someoneman.smartechgdrivefm.utils.Log;

import java.io.IOException;
import java.util.ArrayList;

public class GdriveFile extends AbstractFile {

    public static final String MIME_TYPE_FOLDER = "application/vnd.google-apps.folder";

    private String mFileId; //Идентификатор файла
    private String mWebViewLink; //Ссылка на доступ к файлу через браузер
    private String mMimeType;
    private String mQuery; //Постоянная часть запроса
    private boolean mTrashed; //Флаг перемещения в корзину

    //region Constructors

    public GdriveFile(String id, String query) {
        setName(id);
        setIsDirectory(true);

        mFileId = id;
        mQuery = query;
    }


    public GdriveFile(File file, String query, GdriveFile parent) {
        super(parent);

        setName(file.getName());
        setLastModified(file.getModifiedTime());
        setIsDirectory(file.getMimeType().equals(MIME_TYPE_FOLDER));
        setSize(file.getSize());

        mFileId = file.getId();
        mWebViewLink = file.getWebViewLink();
        mMimeType = file.getMimeType();
        mQuery = query;
        mTrashed = file.getTrashed();
    }

    public GdriveFile(Parcel parcel) {
        super(parcel);

        mFileId = parcel.readString();
        mWebViewLink = parcel.readString();
        mMimeType = parcel.readString();
        mQuery = parcel.readString();
        mTrashed = parcel.readInt() == 1;
    }

    //endregion

    //region Public

    @Override
    public boolean isAsync() {
        return true;
    }

    @Override
    public void open(Context context, Activity activity) {
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(context.getResources().getColor(R.color.colorPrimary));

        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(activity, Uri.parse(mWebViewLink));
    }

    @Override
    public String getHtmlInfo(Context context) {
        return super.getHtmlInfo(context) +
                String.format(
                        context.getString(R.string.gdrive_file_info_dialog_format),

                        mFileId,
                        mMimeType
                );
    }

    @Override
    public boolean delete() {
        if (mTrashed)
            return false;

        try {
            File file = new File();
            file.setTrashed(true);

            Gdrive.getInstance().getDrive().files().update(mFileId, file).execute();
        } catch (IOException e) {
            e.printStackTrace();

            return false;
        }

        return true;
    }

    //endregion

    //region Protected

    @Override
    protected ArrayList<AbstractFile> loadListFiles() {
        String query = makeQuery(mQuery);

        String nextPageToken = "";

        ArrayList<AbstractFile> abstractFiles = new ArrayList<>();

        do {
            try {
                FileList fileList = Gdrive.getInstance().getDrive().files().list()
                        .setPageToken(nextPageToken)
                        .setSpaces("drive")
                        .setQ(query)
                        .setFields("nextPageToken, files(id, name, mimeType, modifiedTime, webViewLink, size, trashed)")
                        .execute();

                nextPageToken = fileList.getNextPageToken();

                for (File file : fileList.getFiles()) {
                    abstractFiles.add(new GdriveFile(file, mQuery, this));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (nextPageToken != null);

        return abstractFiles;
    }

    //endregion

    //region Private

    private String makeQuery(String query) {
        String result = query;

        result = result.replace("$id", mFileId);

        if (mFileId != "root") {
            result = result.replace("$nonRootIdInParents", String.format("'%s' in parents and ", mFileId));
        } else {
            result = result.replace("$nonRootIdInParents", "");
        }

        return result;
    }

    //region Getters

    public String getFileId() {
        return mFileId;
    }

    //endregion

    //endregion

    //region Parcelable

    @Override
    public int describeContents() {
        return 2;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeString(mFileId);
        dest.writeString(mWebViewLink);
        dest.writeString(mMimeType);
        dest.writeString(mQuery);
        dest.writeInt(mTrashed ? 1 : 0);
    }

    public static final Parcelable.Creator<GdriveFile> CREATOR = new Creator<GdriveFile>() {
        @Override
        public GdriveFile createFromParcel(Parcel source) {
            return new GdriveFile(source);
        }

        @Override
        public GdriveFile[] newArray(int size) {
            return new GdriveFile[size];
        }
    };

    //endregion
}
