package com.someoneman.smartechgdrivefm.gdrive.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v7.app.NotificationCompat;
import android.widget.Toast;

import com.someoneman.smartechgdrivefm.R;
import com.someoneman.smartechgdrivefm.gdrive.Gdrive;
import com.someoneman.smartechgdrivefm.gdrive.GdriveFile;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by ultra on 21.06.2016.
 */
public class GdriveFileDownloadService extends IntentService {

    public static final String FILES_PATH_EXTRA = "GdriveFileDownloadServiceFilesPath";

    private static final int NOTIFICATION_ID = 0x1;

    private java.io.File mDocumentsDir;
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mNotificationBuilder;

    public GdriveFileDownloadService() {
        super("GdriveFileDownloadService");
    }


    @Override
    public void onCreate() {
        super.onCreate();

        mDocumentsDir = new java.io.File(Environment.getExternalStorageDirectory().getPath() + "/Documents");
        if (!mDocumentsDir.exists()) {
            if (mDocumentsDir.mkdirs())
                Toast.makeText(getApplicationContext(), "Создан каталог Documents", Toast.LENGTH_LONG).show();

        }

        mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationBuilder = new NotificationCompat.Builder(this);
        mNotificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mNotificationBuilder.setContentTitle(getString(R.string.gdrive_download_service_title));

        Parcelable[] parcelables = intent.getParcelableArrayExtra(FILES_PATH_EXTRA);

        if (!mDocumentsDir.exists()) {
            showDownloadingErrorNotification();
        } else {
            for (int i = 0; i < parcelables.length; i++) {
                GdriveFile gdriveFile = (GdriveFile) parcelables[i];

                mNotificationBuilder
                        .setContentText(gdriveFile.getName())
                        .setProgress(0, 0, true);
                mNotificationManager.notify(NOTIFICATION_ID, mNotificationBuilder.build());

                try {
                    java.io.File outFile = new java.io.File(mDocumentsDir.getAbsolutePath() + "/" + gdriveFile.getName());
                    FileOutputStream fileOutputStream = new FileOutputStream(outFile, true);
                    Gdrive.getInstance().getDrive().files().get(gdriveFile.getFileId()).executeMediaAndDownloadTo(fileOutputStream);

                    mNotificationBuilder.setContentTitle(getString(R.string.gdrive_download_service_success))
                            .setProgress(0, 0, false);
                    mNotificationManager.notify(NOTIFICATION_ID, mNotificationBuilder.build());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    showDownloadingErrorNotification();
                } catch (IOException e) {
                    e.printStackTrace();
                    showDownloadingErrorNotification();
                }
            }
        }
    }

    //region Private

    private void showDownloadingErrorNotification() {
        mNotificationBuilder.setContentTitle(getString(R.string.gdrive_download_service_error))
                .setProgress(0, 0, false);
        mNotificationManager.notify(NOTIFICATION_ID, mNotificationBuilder.build());
    }

    //endregion
}
