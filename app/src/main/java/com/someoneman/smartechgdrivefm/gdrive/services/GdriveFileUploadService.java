package com.someoneman.smartechgdrivefm.gdrive.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.NotificationCompat;
import android.webkit.MimeTypeMap;

import com.google.api.client.http.FileContent;
import com.google.api.services.drive.model.File;
import com.google.common.io.Files;
import com.someoneman.smartechgdrivefm.R;
import com.someoneman.smartechgdrivefm.fs.AbstractFile;
import com.someoneman.smartechgdrivefm.gdrive.Gdrive;
import com.someoneman.smartechgdrivefm.sdcard.SdcardFile;
import com.someoneman.smartechgdrivefm.utils.Log;

import java.io.IOException;

/**
 * Created by ultra on 20.06.2016.
 */
public class GdriveFileUploadService extends IntentService {

    public static final String FILES_PATH_EXTRA = "GdriveFileUploadServiceFilesPath";

    private static final int NOTIFICATION_ID = 0x0;

    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mNotificationBuilder;

    public GdriveFileUploadService() {
        super("GdriveFileUploadService");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationBuilder = new NotificationCompat.Builder(this);
        mNotificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mNotificationBuilder.setContentTitle(getString(R.string.gdrive_upload_service_title));

        Parcelable[] parcelables = intent.getParcelableArrayExtra(FILES_PATH_EXTRA);

        for (int i = 0; i < parcelables.length; i++) {
            AbstractFile abstractFile = (AbstractFile)parcelables[i];

            mNotificationBuilder
                    .setContentText(abstractFile.getName())
                    .setProgress(0, 0, true);
            mNotificationManager.notify(NOTIFICATION_ID, mNotificationBuilder.build());

            File fileMetadata = new File();
            fileMetadata.setName(abstractFile.getName());

            FileContent fileContent = new FileContent(
                    MimeTypeMap.getSingleton().getMimeTypeFromExtension(abstractFile.getExtension()),
                    ((SdcardFile)abstractFile).toFile());
            try {
                File driveFile = Gdrive.getInstance().getDrive().files().create(fileMetadata, fileContent)
                        .setFields("id")
                        .execute();

                mNotificationBuilder.setContentTitle(getString(R.string.gdrive_upload_service_success))
                        .setProgress(0, 0, false);
                mNotificationManager.notify(NOTIFICATION_ID, mNotificationBuilder.build());
            } catch (IOException e) {
                e.printStackTrace();
                showDownloadingErrorNotification();
            }
        }
    }

    //region Private

    private void showDownloadingErrorNotification() {
        mNotificationBuilder.setContentTitle(getString(R.string.gdrive_upload_service_error))
                .setProgress(0, 0, false);
        mNotificationManager.notify(NOTIFICATION_ID, mNotificationBuilder.build());
    }

    //endregion
}
