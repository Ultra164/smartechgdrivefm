package com.someoneman.smartechgdrivefm.gdrive;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.someoneman.smartechgdrivefm.fs.AbstractFile;
import com.someoneman.smartechgdrivefm.gdrive.services.GdriveFileDownloadService;
import com.someoneman.smartechgdrivefm.gdrive.services.GdriveFileUploadService;
import com.someoneman.smartechgdrivefm.utils.Users;

/**
 * Created by ultra on 20.06.2016.
 */
public class Gdrive {

    private static Gdrive ourInstance = new Gdrive();

    private Drive mDrive;

    public static Gdrive getInstance() {
        return ourInstance;
    }

    private Gdrive() {
    }

    //region Public

    public void initDrive(GoogleAccountCredential credential) {
        if (mDrive == null) {
            mDrive = new Drive.Builder(AndroidHttp.newCompatibleTransport(), JacksonFactory.getDefaultInstance(), credential)
                    .setApplicationName("SmartTechGDriveFM")
                    .build();
        } else; //TODO: Add exception
    }

    public void uploadFile(final Context context, final AbstractFile[] files) {
        getDrive(new OnGdriveInit() {
            @Override
            public void onGdriveInit(Drive drive) {
                Toast.makeText(context, "Загрузка файла на Google диск.", Toast.LENGTH_LONG).show();

                Intent intent = new Intent(context, GdriveFileUploadService.class);
                intent.putExtra(GdriveFileUploadService.FILES_PATH_EXTRA, files);
                context.startService(intent);
            }

            @Override
            public void onInitError() {
                Toast.makeText(context, "Не возможно загрузить файл на Google диск. Ошибка авторизации.", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void downloadFile(final Context context, final AbstractFile[] files) {
        getDrive(new OnGdriveInit() {
            @Override
            public void onGdriveInit(Drive drive) {
                Toast.makeText(context, "Загрузка файла c Google диска в Documents.", Toast.LENGTH_LONG).show();

                Intent intent = new Intent(context, GdriveFileDownloadService.class);
                intent.putExtra(GdriveFileDownloadService.FILES_PATH_EXTRA, files);
                context.startService(intent);
            }

            @Override
            public void onInitError() {
                Toast.makeText(context, "Не возможно загрузить файл с Google диска. Ошибка авторизации.", Toast.LENGTH_LONG).show();
            }
        });
    }

    //endregion

    //region Getters

    public Drive getDrive() {
        if (mDrive != null)
            return mDrive;
        else
            return null; //TODO: Add exception
    }

    public void getDrive(final OnGdriveInit onGdriveInit) {
        if (mDrive != null) {
            onGdriveInit.onGdriveInit(mDrive);
        } else {
            if (Users.getInstance().isAuthorized()) {
                initDrive(Users.getInstance().getCredential());

                onGdriveInit.onGdriveInit(mDrive);
            } else {
                Users.getInstance().authorize(new Users.UserAuthorizeListener() {

                    @Override
                    public void onUserAuthorized() {
                        initDrive(Users.getInstance().getCredential());
                        onGdriveInit.onGdriveInit(mDrive);
                    }

                    @Override
                    public void onAuthorizeError() {
                        onGdriveInit.onInitError();
                    }
                });
            }
        }
    }

    //endregion

    //region Listeners

    //endregion

    //region Interfaces

    public interface OnGdriveInit {
        void onGdriveInit(Drive drive);
        void onInitError();
    }

    //endregion
}
