package com.someoneman.smartechgdrivefm.gdrive.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.someoneman.smartechgdrivefm.R;
import com.someoneman.smartechgdrivefm.dialogs.FileOptionsDialog;
import com.someoneman.smartechgdrivefm.fs.AbstractFile;
import com.someoneman.smartechgdrivefm.fs.AbstractFileFragment;
import com.someoneman.smartechgdrivefm.gdrive.Gdrive;
import com.someoneman.smartechgdrivefm.gdrive.GdriveFile;
import com.someoneman.smartechgdrivefm.utils.Log;
import com.someoneman.smartechgdrivefm.utils.Users;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ultra on 18.06.2016.
 */
public class GdriveFilesFragment extends AbstractFileFragment  {

    //region Fragment overrides

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (Users.getInstance().isAuthorized()) {
            initList();
        } else {
            Users.getInstance().authorize(mUserAuthorizeListener);
        }
    }

    //endregion

    //region Protected

    @Override
    protected String getRootTitle() {
        return getResources().getString(R.string.gdrive_mydrive);
    }

    @Override
    protected void showWorkWithFileDialog(AbstractFile abstractFile, int position) {
        FileOptionsDialog dialog = createWorkWithFileDialogBuilder(abstractFile, position);

        if (!abstractFile.isDirectory()) {
            ArrayList<CharSequence> items = new ArrayList<>();
            items.addAll(Arrays.asList(dialog.getItems()));
            items.addAll(Arrays.asList(getResources().getStringArray(R.array.gdriveFileDialogOptions)));

            dialog.setItems(items.toArray(new CharSequence[items.size()]));
        }

        dialog.show();
    }

    @Override
    protected void selectFileOptions(int which, AbstractFile abstractFile) {
        switch (which) {
            case 0: //Загрузка с Google диска в каталог Documents
                Gdrive.getInstance().downloadFile(getContext(), new AbstractFile[] {abstractFile});

                break;
        }
    }

    //endregion

    //region Private

    private void initList() {
        if (Gdrive.getInstance().getDrive() == null)
            Gdrive.getInstance().initDrive(Users.getInstance().getCredential());

        if (!isHasSavedInstanceState())
            changeDir(new GdriveFile("root", getString(R.string.gdrive_query_storage)));
    }

    //endregion

    //region Listeners

    private Users.UserAuthorizeListener mUserAuthorizeListener = new Users.UserAuthorizeListener() {
        @Override
        public void onUserAuthorized() {
            initList();
        }

        @Override
        public void onAuthorizeError() {
            Log.I("ERROR!");
        }
    };

    //endregion
}
