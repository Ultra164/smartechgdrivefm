package com.someoneman.smartechgdrivefm.gdrive.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.someoneman.smartechgdrivefm.R;
import com.someoneman.smartechgdrivefm.fs.AbstractFileFragment;
import com.someoneman.smartechgdrivefm.gdrive.Gdrive;
import com.someoneman.smartechgdrivefm.gdrive.GdriveFile;
import com.someoneman.smartechgdrivefm.utils.Log;
import com.someoneman.smartechgdrivefm.utils.Users;

/**
 * Created by ultra on 20.06.2016.
 */
public class GdriveSharedWithMeFilesFragment extends AbstractFileFragment {

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (Users.getInstance().isAuthorized()) {
            initList();
        } else {
            Users.getInstance().authorize(mUserAuthorizeListener);
        }
    }

    @Override
    protected String getRootTitle() {
        return getResources().getString(R.string.gdrive_multiple_acceess);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void initList() {
        if (Gdrive.getInstance().getDrive() == null)
            Gdrive.getInstance().initDrive(Users.getInstance().getCredential());

        if (!isHasSavedInstanceState())
            changeDir(new GdriveFile("root", getString(R.string.gdrive_quert_shared_with_me)));
    }

    //region Listeners

    private Users.UserAuthorizeListener mUserAuthorizeListener = new Users.UserAuthorizeListener() {
        @Override
        public void onUserAuthorized() {
            initList();
        }

        @Override
        public void onAuthorizeError() {
            Log.I("ERROR!");
        }
    };

    //endregion

}