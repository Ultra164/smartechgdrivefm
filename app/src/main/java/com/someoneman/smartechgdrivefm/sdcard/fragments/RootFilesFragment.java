package com.someoneman.smartechgdrivefm.sdcard.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.someoneman.smartechgdrivefm.fs.AbstractFileFragment;
import com.someoneman.smartechgdrivefm.sdcard.SdcardFile;

/**
 * Created by ultra on 19.06.2016.
 */
public class RootFilesFragment extends AbstractFileFragment {

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (!isHasSavedInstanceState())
            changeDir(new SdcardFile("/"));
    }

    @Override
    protected String getRootTitle() {
        return "/";
    }
}
