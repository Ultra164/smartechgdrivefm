package com.someoneman.smartechgdrivefm.sdcard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;
import android.text.Spanned;
import android.webkit.MimeTypeMap;

import com.google.api.client.util.DateTime;
import com.someoneman.smartechgdrivefm.R;
import com.someoneman.smartechgdrivefm.fs.AbstractFile;
import com.someoneman.smartechgdrivefm.utils.Consts;
import com.someoneman.smartechgdrivefm.utils.Log;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by ultra on 16.06.2016.
 */
public class SdcardFile extends AbstractFile {

    //region Vars

    private String mAbsolutePath;

    //endregion

    //region Constructors

    public SdcardFile(String path) {
        this(new File(path));
    }

    public SdcardFile(File file) {
        this(file, null);
    }

    public SdcardFile(File file, SdcardFile parent) {
        super(parent);

        setName(file.getName());
        setLastModified(file.lastModified());
        setIsDirectory(file.isDirectory());
        setIsSystem(checkIsSystem(getName()));
        setSize(file.length());

        mAbsolutePath = file.getAbsolutePath();
    }

    public SdcardFile(Parcel parcel) {
        super(parcel);

        mAbsolutePath = parcel.readString();
    }

    //endregion

    //region Public

    @Override
    public void open(Context context, Activity activity) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(
                Uri.fromFile(new File(mAbsolutePath)),
                MimeTypeMap.getSingleton().getMimeTypeFromExtension(getExtension())
        );

        activity.startActivity(intent);
    }

    @Override
    public String getHtmlInfo(Context context) {
        return super.getHtmlInfo(context) +
                String.format(
                        context.getString(R.string.sdcard_file_info_dialog_format),

                        mAbsolutePath
                );
    }

    @Override
    public boolean delete() {
        return new File(mAbsolutePath).delete();
    }

    public File toFile() {
        return new File(mAbsolutePath);
    }

    public String getAbsolutePath() {
        return mAbsolutePath;
    }

    //endregion

    //region Protected

    @Override
    protected ArrayList<AbstractFile> loadListFiles() {
        File[] files = new File(mAbsolutePath).listFiles();
        ArrayList<AbstractFile> abstractFiles = new ArrayList<>();

        if (files == null)
            return null;

        for (int i = 0; i < files.length; i++) {
            AbstractFile abstractFile = new SdcardFile(files[i], this);

            if (abstractFile.isSystem() && !Consts.SHOW_SYSTEMS)
                continue;

            abstractFiles.add(abstractFile);
        }

        return abstractFiles;
    }

    //endregion

    //region Parcelable

    @Override
    public int describeContents() {
        return 1;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeString(mAbsolutePath);
    }

    public static final Parcelable.Creator<SdcardFile> CREATOR = new Creator<SdcardFile>() {
        @Override
        public SdcardFile createFromParcel(Parcel source) {
            return new SdcardFile(source);
        }

        @Override
        public SdcardFile[] newArray(int size) {
            return new SdcardFile[size];
        }
    };

    //endregion

    //region Statics

    public static boolean checkIsSystem(String name) {
        return false;
    }

    //endregion
}
