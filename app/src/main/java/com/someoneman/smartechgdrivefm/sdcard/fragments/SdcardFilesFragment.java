package com.someoneman.smartechgdrivefm.sdcard.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.someoneman.smartechgdrivefm.R;
import com.someoneman.smartechgdrivefm.dialogs.FileOptionsDialog;
import com.someoneman.smartechgdrivefm.fs.AbstractFile;
import com.someoneman.smartechgdrivefm.fs.AbstractFileFragment;
import com.someoneman.smartechgdrivefm.gdrive.Gdrive;
import com.someoneman.smartechgdrivefm.sdcard.SdcardFile;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ultra on 17.06.2016.
 */
public class SdcardFilesFragment extends AbstractFileFragment {

    private static final int DEVICE_STORAGE_PERMISSIONS_REQUEST_CODE = 0x0;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (isPermissionsGranted()) {
            if (!isHasSavedInstanceState())
                initList();
        } else {
            requestPermissions(false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case DEVICE_STORAGE_PERMISSIONS_REQUEST_CODE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++)
                        if (grantResults[i] == PackageManager.PERMISSION_DENIED)
                            return;

                    if (!isHasSavedInstanceState())
                        initList();
                }

                break;
        }
    }

    private void initList() {
        AbstractFile t = new SdcardFile(Environment.getExternalStorageDirectory().getPath() + "/");

        changeDir(t);
    }

    private boolean isPermissionsGranted() {
        return ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions(boolean accept) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) && !accept) {
            showRationaleDialog();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, DEVICE_STORAGE_PERMISSIONS_REQUEST_CODE);
        }
    }

    private void showRationaleDialog() {
        new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.permission_denied))
                .setMessage(getString(R.string.external_storage_permission_rationale_dialog_text))
                .setPositiveButton(getString(R.string.accept), rationDialogClickListener)
                .setNegativeButton(getString(R.string.cancel), null).show();
    }

    private void noPermissionError() {
        getActionBar().setTitle("Отсутствует доступ");
    }

    private DialogInterface.OnClickListener rationDialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    requestPermissions(true);

                    break;
            }
        }
    };

    @Override
    protected String getRootTitle() {
        return getResources().getString(R.string.device_sdcard);
    }

    @Override
    protected void showWorkWithFileDialog(AbstractFile abstractFile, int position) {
        FileOptionsDialog dialog = createWorkWithFileDialogBuilder(abstractFile, position);

        if (!abstractFile.isDirectory()) {
            ArrayList<CharSequence> items = new ArrayList<>();
            items.addAll(Arrays.asList(dialog.getItems()));
            items.addAll(Arrays.asList(getResources().getStringArray(R.array.sdcardFileDialogOptions)));

            dialog.setItems(items.toArray(new CharSequence[items.size()]));
        }

        dialog.show();
    }

    @Override
    public void selectFileOptions(int which, AbstractFile abstractFile) {
        switch (which) {
            case 0: //Загрузка в корневой каталог Goolge диска
                Gdrive.getInstance().uploadFile(getContext(), new AbstractFile[] {abstractFile});

                break;
        }
    }
}
