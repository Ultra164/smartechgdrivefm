package com.someoneman.smartechgdrivefm.utils;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.drive.DriveScopes;
import com.someoneman.smartechgdrivefm.R;

import java.util.Arrays;

/**
 * Created by ultra on 19.06.2016.
 */
public class Users implements GoogleApiClient.OnConnectionFailedListener {

    private static Users ourInstance = new Users();

    private static final String[] SCOPES = {DriveScopes.DRIVE, DriveScopes.DRIVE_FILE};
    private static final String PREFERENCES = "Users";
    private static final String PREFERENCES_USERNAME = "Users.Username";

    public static final int GET_ACCOUNTS_PERMISSION_REQUEST_CODE = 0x1;
    public static final int GOOGLE_ACCOUNT_AUTHORIZE_ACTIVITY_RESULT_CODE = 0x1;

    private Context mContext;
    private FragmentActivity mActivity;
    private UserAuthorizeListener mUserAuthorizeListener;
    private GoogleAccountCredential mCredential;
    private GoogleApiClient mGoogleApiClient;

    public static Users getInstance() {
        return ourInstance;
    }

    //region Constructors

    private Users() {
    }

    public void init(Context context, FragmentActivity activity) {
        mContext = context;
        mActivity = activity;

        mCredential = GoogleAccountCredential.usingOAuth2(mContext, Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        String accountName = sharedPreferences.getString(PREFERENCES_USERNAME, null);

        if (accountName != null)
            mCredential.setSelectedAccountName(accountName);
    }

    //endregion

    //region Public

    public void authorize(UserAuthorizeListener userAuthorizeListener) {
        mUserAuthorizeListener = userAuthorizeListener;

        if (isPermissionsGranted()) {
            authorize();
        } else {
            requestPermissions(false);
        }
    }

    private void authorize() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestId()
                .requestProfile()
                .requestScopes(
                        new Scope(DriveScopes.DRIVE),
                        new Scope(DriveScopes.DRIVE_FILE)
                )
                .build();

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                    .enableAutoManage(mActivity, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }

        Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        mActivity.startActivityForResult(intent, GOOGLE_ACCOUNT_AUTHORIZE_ACTIVITY_RESULT_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case GOOGLE_ACCOUNT_AUTHORIZE_ACTIVITY_RESULT_CODE:
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);

                break;
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == GET_ACCOUNTS_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        authorizationError();

                        return;
                    }

                    authorize();
                }
            } else {
                authorizationError();
            }
        } else {
            //TODO: add exception?
        }
    }

    //endregion

    //region Getters

    public GoogleAccountCredential getCredential() {
        return mCredential;
    }

    //endregion

    //region Private

    private void requestPermissions(boolean accept) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.GET_ACCOUNTS) && !accept) {
            showRationaleDialog();
        } else {
            ActivityCompat.requestPermissions(mActivity, new String[] {Manifest.permission.GET_ACCOUNTS}, GET_ACCOUNTS_PERMISSION_REQUEST_CODE);
        }
    }

    private void showRationaleDialog() {
        new AlertDialog.Builder(mContext)
                .setTitle(mContext.getString(R.string.permission_denied))
                .setMessage(mContext.getString(R.string.get_contacts_permission_rationale_dialog_text))
                .setPositiveButton(mContext.getString(R.string.accept), rationDialogClickListener)
                .setNegativeButton(mContext.getString(R.string.cancel), rationDialogClickListener).show();
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();

            mCredential.setSelectedAccountName(account.getEmail());

            authorizationSuccess();
        } else {
            authorizationError();
        }
    }

    private void authorizationError() {
        if (mUserAuthorizeListener != null) {
            mUserAuthorizeListener.onAuthorizeError();

            mUserAuthorizeListener = null;
        }
    }

    private void authorizationSuccess() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(PREFERENCES_USERNAME, getCredential().getSelectedAccountName());
        editor.apply();

        if (mUserAuthorizeListener != null) {
            mUserAuthorizeListener.onUserAuthorized();

            mUserAuthorizeListener = null;
        }
    }

    //endregion

    //region Params

    public boolean isAuthorized() {
        return mCredential.getSelectedAccountName() != null;
    }

    private boolean isPermissionsGranted() {
        return ContextCompat.checkSelfPermission(mContext, Manifest.permission.GET_ACCOUNTS)
                == PackageManager.PERMISSION_GRANTED;
    }

    //endregion

    //region Listeners

    private DialogInterface.OnClickListener rationDialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    requestPermissions(true);

                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    authorizationError();

                    break;
            }
        }
    };


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mUserAuthorizeListener.onAuthorizeError();
    }

    //endregion

    //region Interfaces

    public interface UserAuthorizeListener {
        void onUserAuthorized();
        void onAuthorizeError();
    }

    //endregion
}
